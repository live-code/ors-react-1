import React  from 'react';
import { useInterceptor } from './useInterceptor';

export const Interceptor: React.FC = () => {
  const { error } = useInterceptor();
  return error ? <div>error</div> : null
};
