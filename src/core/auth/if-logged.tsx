import { isLogged } from './authentication.service';
import React from 'react';

export const IfLogged: React.FC = (props) => {
  return isLogged() ? <>{props.children}</> : null;
}
