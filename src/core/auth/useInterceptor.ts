import { useEffect, useState } from 'react';
import axios from 'axios';
import { getItemFromLocalStorage } from './localstorage.helper';

export const useInterceptor = () => {
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    console.log('useInterceptor', error)
    // Add a request interceptor
    axios.interceptors.request.use(function (config) {
      // Do something before request is sent
      const cloned = {
        ...config,
        headers: {
          ...config.headers,
          'AuthJWT': 'bearer ' + getItemFromLocalStorage('token')
        }
      }
      setError(false);
      return cloned;
    }, function (error) {
      // Do something with request error
      return Promise.reject(error);
    });

    // Add a response interceptor
    axios.interceptors.response.use(function (response) {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      return response;
    }, function (error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      console.log('error a true')
      setError(true);

      return Promise.reject(error);
    });

  }, []);

  return {
    error
  }
}
