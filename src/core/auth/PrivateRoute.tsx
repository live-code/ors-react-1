import React from 'react';
import { Redirect, Route } from 'react-router';
import { isLogged } from './authentication.service';
import { RouteProps } from 'react-router-dom';


export const PrivateRoute: React.FC<RouteProps> = ({ children, ...rest }) => {
  return (
    <Route {...rest}>
      {
        isLogged() ?
          children :
          <Redirect to={{ pathname: "/login" }} />
      }
    </Route>
  );
};
