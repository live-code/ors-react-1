import React  from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import { IfLogged } from '../auth/if-logged';
import { useNavBar } from './hooks/use-navbar';

export const NavBar: React.FC = () => {
  const { actions, state } = useNavBar();
  // const { pathname } = useLocation();

  console.log('navbar')

  return (
    <nav className="navbar navbar-expand navbar-light bg-light">
      <div className="navbar-brand">
        <NavLink to="/login">React Auth</NavLink>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/login">Login</NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/home">Home</NavLink>
          </li>

          <IfLogged>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin">Admin</NavLink>
            </li>
          </IfLogged>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              exact={ true }
              className="nav-link"
              to="/settings">Settings</NavLink>
          </li>

          <li>
            <NavLink
              className="nav-link"
              activeClassName="active"
              to="/settings/1"
            >
              Router with params
            </NavLink>
          </li>
          <li onClick={actions.quit}>
            quit
          </li>

        </ul>
      </div>
    </nav>
  )

};
