import { useHistory } from 'react-router';
import { signOut } from '../../auth/authentication.service';
import { useState } from 'react';

export function useNavBar() {
  const [pippo, setPippo] = useState();
  const history = useHistory();

  function signOutHandler() {
    signOut();
    history.push('/login');
  }

  function doSomethingHandler() {
    // doSomething(....)
  }

  return {
    actions: {
      quit: signOutHandler,
    },
    state: pippo
  }
}
