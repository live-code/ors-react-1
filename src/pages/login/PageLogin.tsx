import React from 'react';
import { Link, Redirect, Route, RouteComponentProps } from 'react-router-dom';
import { SignIn } from './signin/SignIn';


export const PageLogin: React.FC<RouteComponentProps> = ({ match }) => {
  return <div>

    <Route path={`${match.path}/:sectionName`} component={BreadCrumbs}/>

    <Route path={`${match.path}/signin`}>
      <SignIn />
    </Route>

    <Route path="/login/lostpass">
      <input type="text"/>
      <hr/>
    </Route>

    <Route path="/login" exact>
      <Redirect to="/login/signin" />
    </Route>

    <strong>
      <Link to={`${match.path}/signin`}>Login</Link> |
      <Link to="/login/lostpass">LostPass</Link>
    </strong>

  </div>
}


const BreadCrumbs = (props: RouteComponentProps<{ sectionName: string }>) => {
  console.log('props', props)
  return <div>
    <em>login / {props.match.params.sectionName}</em>
  </div>
}
