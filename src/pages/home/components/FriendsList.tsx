import React  from 'react';
import { Friend } from '../../../model/friend';
import { FriendListItem } from './FriendsListItem';

interface FriendsListProps {
  friends: Friend[];
  onDelete: (user: Friend) => void
}

export const FriendList: React.FC<FriendsListProps> = (props) => {
  return <ul className="list-group">
    {
      props.friends.map((item: Friend, index: number) => {
        return (
          <FriendListItem
            key={item.id}
            friend={item}
            index={index}
            onDelete={() => props.onDelete(item)}
          />
        )
      })
    }
  </ul>
};
