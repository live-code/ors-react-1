import React from 'react';
import { Friend } from '../../../model/friend';

export const Total: React.FC<{ friends: Friend[]}> = props => {
  const getTotal = () => {
    return props.friends.reduce((acc: number, item: Friend) => {
      return acc + item.tweets;
    }, 0)
  };
  return <div>TOTALE TWEETS: {getTotal()}</div>
}
