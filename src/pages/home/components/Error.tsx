import React from 'react';

export const Error: React.FC<{ hasError: boolean}> = ({hasError}) => {
  return hasError ?  <div className="alert alert-danger">Server error</div> : null;
}

