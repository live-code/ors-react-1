import React  from 'react';
import { Friend } from '../../../model/friend';

interface FriendsListItemProps {
  index: number;
  friend: Friend;
  onDelete: () => void
}

export const FriendListItem: React.FC<FriendsListItemProps> = (props) => {
  return (
    <li key={props.friend.id} className="list-group-item">
      {props.index + 1}. {props.friend.name} - {props.friend.tweets}
      <button
        onClick={props.onDelete}>Del</button>
    </li>
  )
};
