import React from 'react';
import { Total } from './components/Total';
import { Error } from './components/Error';
import { FriendList } from './components/FriendsList';
import { useFriends } from './hooks/use-friends';


export default function PageHome() {
  const { error, friends, actions } = useFriends();

  return (
    <div>
      <FriendList friends={friends}
                  onDelete={actions.delete} />
      <Total friends={friends}/>
      <Error hasError={error} />
    </div>
  )
}
