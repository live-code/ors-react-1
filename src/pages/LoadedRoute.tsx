import React, { useEffect, useState } from 'react';
import { Route, RouteProps } from 'react-router-dom';

interface LoaderRouteProps {
  url: string;
}

export const LoaderRoute: React.FC<LoaderRouteProps & RouteProps> = ({ url, children, ...rest }) => {
  const [data, setData] = useState<number[]>();

  const c: any[] = React.Children.toArray(children);
  const component: any = React.createElement(c[0].type, { data }, 'ciao')

  useEffect(() => {
    // rest...
    setData([1, 2, 3]);
  }, [])

  return <Route {...rest}>
    {component}
  </Route>
};
