import React, { lazy, Suspense } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
// import { PageHome } from './pages/home/PageHome';
import { PageSettings } from './pages/PageSettings';
import { PageLogin } from './pages/login/PageLogin';
import { PageAdmin } from './pages/PageAdmin';
import { LoaderRoute } from './pages/LoadedRoute';
import { NavBar } from './core/components/NavBar';
import { PageSettingsDetails } from './pages/PageSettingsDetails';
import { PrivateRoute } from './core/auth/PrivateRoute';
import { useInterceptor } from './core/auth/useInterceptor';
import { Interceptor } from './core/auth/Interceptor';

const PageHome = lazy(() => import('./pages/home/PageHome'))

function App() {
  // ....
  return (
      <BrowserRouter>
        <Interceptor />
        <Route path="*" component={NavBar} />
        <Suspense fallback={<div>Loading...</div>}>
          <Switch>
            <Route path="/home">
              <PageHome />
            </Route>
            <Route path="/settings" exact>
              <PageSettings />
            </Route>

            <Route
              path="/settings/:id"
              component={PageSettingsDetails}
            />


            <PrivateRoute path="/admin">
              <PageAdmin />
            </PrivateRoute>

            {/*<LoaderRoute
              path="/admin" url='...'>
              <PageAdmin />
            </LoaderRoute>*/}

            <Route path="/login" component={PageLogin} />
            <Route path="*">
              Pagina di default
            </Route>
          </Switch>
        </Suspense>

      </BrowserRouter>
  );
}

export default App;
